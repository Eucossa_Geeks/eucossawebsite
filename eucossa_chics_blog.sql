-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 04, 2016 at 10:28 PM
-- Server version: 5.6.23-log
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eucossa_chics_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--


CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass_word` varchar(255) NOT NULL,
  `password2` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `username`, `pass_word`, `password2`) VALUES
(60, 'Chao', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(61, 'whitney c', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(62, 'lucy', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(65, 'steve', '5ad80c9aa5db8dcee45281cd9dafaea1', '5ad80c9aa5db8dcee45281cd9dafaea1'),
(69, 'ethredah chao', '13bbb5fe71090e0a04e2dba61e77d870', '13bbb5fe71090e0a04e2dba61e77d870'),
(70, 'moni', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(72, 'lady', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(74, 'winnie', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(75, 'fiona', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(76, 'clere', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(77, 'drake', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(78, 'milly', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(80, 'elizabeth', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(81, 'june', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(82, 'lex', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(83, 'deedee', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(84, 'lydia', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(85, 'lily', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(86, 'root', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(87, 'mary', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(88, 'slim', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(89, 'shatheedi sairuji', 'ca794fb2d950acf25c964ecc35f2d7e2', 'ca794fb2d950acf25c964ecc35f2d7e2'),
(90, 'claris', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(91, 'ninja', '3899dcbab79f92af727c2190bbd8abc5', '3899dcbab79f92af727c2190bbd8abc5'),
(92, 'mlamba', '0ba9aabe04d5d34b0a2c5f12650d0077', '0ba9aabe04d5d34b0a2c5f12650d0077'),
(93, 'guru', '73005d28babc7a958a1362a2201686de', '73005d28babc7a958a1362a2201686de'),
(94, 'admin', 'b248e08d5c23541514558eea059c08cf', 'b248e08d5c23541514558eea059c08cf'),
(95, 'thecrazyone', '5d41402abc4b2a76b9719d911017c592', '5d41402abc4b2a76b9719d911017c592'),
(96, 'geekgirl', 'e0de1b920aae401c1a1563eee916f1b8', 'e0de1b920aae401c1a1563eee916f1b8'),
(97, 'mehere', '2fb5a510d464062f48512ba807173b8e', '2fb5a510d464062f48512ba807173b8e'),
(98, 'geekly', 'e0de1b920aae401c1a1563eee916f1b8', 'e0de1b920aae401c1a1563eee916f1b8'),
(99, 'mark', '75c747803be781ef76eed8ca863a9e36', '75c747803be781ef76eed8ca863a9e36'),
(100, 'name', '73415b57d89777f374cb6568a239477e', '73415b57d89777f374cb6568a239477e'),
(101, 'fatty', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(102, 'num', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(103, 'admin1', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99'),
(104, 'meme', '1a1dc91c907325c69271ddf0c944bc72', '1a1dc91c907325c69271ddf0c944bc72'),
(105, 'someone', '5f4dcc3b5aa765d61d8327deb882cf99', '5f4dcc3b5aa765d61d8327deb882cf99');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `eventdate` datetime NOT NULL,
  `from` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `username`, `message`, `eventdate`, `from`) VALUES
(2, 'steve', 'hello there! Greetings from your friend . ME :)', '2015-03-27 11:20:18', 'ethredah chao'),
(4, 'ethredah chao', 'It is god to hear from you again.', '2015-03-27 11:23:36', 'steve'),
(5, 'zeph', 'Hello Zeph :)', '2015-03-27 11:39:35', 'ethredah chao'),
(6, 'steve', 'Hello  Steve   :)', '2015-03-27 11:42:24', 'ethredah chao'),
(7, 'ethredah chao', 'Hello there Ethy', '2015-03-27 11:43:09', 'steve'),
(9, 'ethredah chao', 'Hello there...greetings from the administrator', '2015-03-27 12:03:11', 'admin'),
(10, 'admin', 'Heeloo admin....you are such a bully', '2015-03-27 12:04:25', 'ethredah chao'),
(11, 'babu', 'hi', '2015-03-27 12:16:18', 'admin'),
(12, 'steve', 'howdy my man :)', '2015-03-27 12:19:22', 'admin'),
(14, 'admin', 'Hello there...am trying to study for my cats buh am having trouble :(', '2015-03-27 17:34:14', 'ethredah chao'),
(16, 'ethredah chao', 'Hey', '2015-03-27 18:42:39', 'admin'),
(17, 'ethredah chao', 'Hi there am just testing this thingy :)\r\n\r\nHope you enjoy this silly chat', '2015-03-29 10:59:25', 'admin'),
(20, 'fancy', 'Hello there Fancy... fancy name i see. Welcome to the group. I am Chao.', '2015-04-02 08:37:08', 'chao'),
(21, 'chao', 'Hey thanx i feel gud', '2015-04-02 08:39:27', 'fancy'),
(24, 'moni', 'hello', '2015-04-02 10:20:02', 'ethredah chao'),
(26, 'chao', 'hi', '2015-04-06 16:48:58', 'admin'),
(27, 'chao', 'hi', '2015-04-06 16:48:58', 'admin'),
(28, 'admin', 'This is root...hello there', '2015-04-07 12:36:31', 'root'),
(29, 'chao', 'hello there', '2015-04-11 09:24:50', 'admin'),
(30, 'admin', 'hello am a new member', '2015-09-25 14:24:24', 'geekgirl'),
(31, 'geekgirl', 'hello there', '2015-09-25 14:23:33', 'admin'),
(32, 'geekgirl', 'hi', '2015-09-25 14:51:04', 'admin'),
(33, 'geekgirl', 'hi again', '2015-09-25 14:52:48', 'admin'),
(34, 'geekgirl', 'hi again', '2015-09-25 14:52:48', 'admin'),
(35, 'mark', 'Hello there!\r\n', '2016-04-03 13:23:09', 'name'),
(36, 'fatty', 'Hello\r\nThere.', '2016-04-24 19:20:41', 'num'),
(37, 'fatty', 'Hello\r\nThere. Again.', '2016-04-24 19:20:41', 'num'),
(38, 'fatty', 'Hello\r\nThere. Again.\r\nAm new here. Fatty.', '2016-04-24 19:20:41', 'num'),
(39, 'fatty', 'Hello\r\nThere. Again.\r\nAm new here. Fatty.\r\nI need some help!!', '2016-04-24 19:20:41', 'num');

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE IF NOT EXISTS `other` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `eventdate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other`
--

INSERT INTO `other` (`id`, `username`, `content`, `eventdate`) VALUES
(5, 'lucy', 'Hmmm...i got nothing to say people!!', '0000-00-00 00:00:00'),
(9, 'chao', 'today is a great day people', '2024-03-15 00:00:00'),
(10, 'chao', 'what to write....i really have no idea', '2024-03-15 00:00:00'),
(11, 'zeph', 'zephaniah', '2024-03-15 00:00:00'),
(12, 'steve', 'This is a great site :)', '2024-03-15 00:00:00'),
(13, 'steve', 'this is a just a count add', '2024-03-15 00:00:00'),
(14, 'chao', 'i have posted this', '2024-03-15 00:00:00'),
(15, 'chao', 'the recent quote for now', '2024-03-15 00:00:00'),
(16, 'whitney c', 'My official quote', '2024-03-15 00:00:00'),
(17, 'whitney c', 'hmmm,,,lemmi see', '2024-03-15 00:00:00'),
(18, 'whitney c', 'trying once more', '2024-03-15 00:00:00'),
(19, 'whitney c', 'time testing', '2015-03-24 16:59:59'),
(20, 'whitney c', 'time testing again', '2015-03-24 17:00:17'),
(22, 'chao', 'This is even more craizy', '2015-03-25 17:05:34'),
(23, 'chao', 'Share cool tech stuff that will motivate the ladies looking at this blog. U have to have an account to share something, so if you are new here, SignUp and if you already have an account,', '2015-03-26 09:54:45'),
(24, 'ethredah chao', 'Hello there..this is my other post', '2015-03-26 14:07:38'),
(30, 'ethredah chao', 'hello there', '2015-04-02 10:05:07'),
(31, 'ethredah chao', 'hello there', '2015-04-02 10:06:12'),
(32, 'ethredah chao', 'hello there', '2015-04-02 10:06:12'),
(33, 'ethredah chao', 'hello there', '2015-04-02 10:06:12'),
(34, 'ethredah chao', 'hello there', '2015-04-02 10:06:12'),
(36, 'ethredah chao', 'hello there', '2015-04-02 10:06:12'),
(38, 'chao', 'I am feeling inspiring', '2015-04-02 10:10:37'),
(39, 'ethredah chao', 'cool', '2015-04-02 10:20:42'),
(40, 'admin', 'This is good thing\r\n', '2015-04-06 13:41:47'),
(43, 'chao', 'what the hell?', '2015-04-06 17:00:12'),
(44, 'admin', 'this\r\n', '2015-09-25 14:03:49'),
(45, 'geekgirl', 'insiring stuff', '2015-09-25 14:34:14'),
(46, 'admin', 'hello there', '2015-09-25 14:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `eventdate` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `username`, `content`, `eventdate`) VALUES
(76, 'lucy', 'what the hell', '2024-03-15 00:00:00'),
(89, 'admin', 'Am the boss!!', '2024-03-15 00:00:00'),
(91, 'whitney c', 'My official post', '2024-03-15 00:00:00'),
(93, 'whitney c', 'trying once more', '2015-03-24 16:59:13'),
(98, 'ethredah chao', 'Hello there..this is my post', '2015-03-26 14:07:38'),
(101, 'admin', 'I am the admin...so am the boss!!\r\n:p', '2015-03-26 16:46:54'),
(102, 'chao', 'I think am getting addicted to this stuff\r\nThis is so much fun :)', '2015-03-26 22:13:57'),
(103, 'ethredah chao', 'This is awesome!.....I am chatting with someone ryt now :)', '2015-03-27 11:27:50'),
(104, 'steve', 'coolest thing ever', '2015-03-27 11:31:20'),
(117, 'fancy', 'Hey am so fancy..you already know', '2015-04-02 08:38:09'),
(118, 'chao', 'Awesome', '2015-04-02 09:16:32'),
(119, 'ethredah chao', 'hello there', '2015-04-02 09:43:59'),
(120, 'ethredah chao', 'hello there', '2015-04-02 09:45:18'),
(121, 'ethredah chao', 'hello there again', '2015-04-02 09:45:46'),
(122, 'ethredah chao', 'hello there again', '2015-04-02 09:54:40'),
(123, 'ethredah chao', 'this is awesome\r\n', '2015-04-02 09:54:44'),
(124, 'ethredah chao', 'this is awesome\r\n', '2015-04-02 09:54:44'),
(125, 'ethredah chao', 'hello there again', '2015-04-02 09:54:40'),
(126, 'ethredah chao', 'some post', '2015-04-02 10:04:12'),
(127, 'ethredah chao', 'something', '2015-04-02 10:04:21'),
(130, 'chao', 'Helo there...this is something cool', '2015-04-02 10:10:19'),
(131, 'chao', 'Helo there...this is something cool', '2015-04-02 10:10:19'),
(132, 'chao', 'Helo there...this is something cool', '2015-04-02 10:13:51'),
(133, 'chao', 'Helo there...this is something cool', '2015-04-02 10:13:51'),
(134, 'chao', 'Helo there...this is something cool', '2015-04-02 10:13:51'),
(165, 'admin', 'am sharing something cool', '2015-04-02 14:52:52'),
(166, 'admin', 'am sharing something cool', '2015-04-02 14:52:52'),
(167, 'admin', 'Something i just have to post', '2015-04-06 13:42:15'),
(169, 'admin', 'hi', '2015-04-06 16:11:17'),
(170, 'admin', 'test', '2015-04-06 16:12:33'),
(172, 'admin', 'testing this', '2015-04-06 16:13:34'),
(173, 'admin', 'testing this stuff', '2015-04-06 16:15:09'),
(174, 'admin', 'testing this stuff', '2015-04-06 16:15:09'),
(175, 'admin', 'hope it works', '2015-04-06 16:33:22'),
(176, 'admin', 'hope it works hhhkhjjhj', '2015-04-06 16:33:22'),
(183, 'chao', 'share soomething', '2015-04-06 16:59:54'),
(185, 'ninja', 'howdoes thing work?', '2015-04-22 11:39:54'),
(186, 'mlamba', 'share', '2015-05-25 18:27:23'),
(187, 'admin', 'hey there', '2015-09-25 13:18:19'),
(188, 'admin', 'hey there', '2015-09-25 13:25:22'),
(189, 'admin', 'hey there again', '2015-09-25 13:27:06'),
(190, 'admin', 'hey there again', '2015-09-25 13:27:35'),
(191, 'admin', '0', '0000-00-00 00:00:00'),
(192, 'admin', '0', '0000-00-00 00:00:00'),
(194, 'admin', 'something\r\nanother\r\none', '2015-09-25 14:00:15'),
(195, 'admin', 'something\r\nanother\r\none', '2015-09-25 14:00:15'),
(196, 'admin', 'something\r\nanother\r\none', '2015-09-25 14:00:15'),
(197, 'admin', 'something\r\nanother\r\none', '2015-09-25 14:00:15'),
(198, 'admin', 'something', '2015-09-25 14:03:23'),
(199, 'admin', 'this is a post', '2015-09-25 14:04:22'),
(200, 'admin', 'this is a post', '2015-09-25 14:04:22'),
(201, 'admin', 'gghh', '2015-09-25 14:10:53'),
(202, 'admin', 'gghh', '2015-09-25 14:10:53'),
(203, 'admin', 'gghh', '2015-09-25 14:10:53'),
(204, 'admin', 'gghh', '2015-09-25 14:10:53'),
(205, 'admin', 'gghh', '2015-09-25 14:10:53'),
(206, 'admin', 'gggehehe', '2015-09-25 14:15:21'),
(207, 'admin', 'ggheheghbfbmbmfwe', '2015-09-25 14:15:49'),
(208, 'admin', 'ggheheghbfbmbmfwe', '2015-09-25 14:15:49'),
(209, 'admin', 'ggheheghbfbmbmfwe', '2015-09-25 14:15:49'),
(210, 'admin', 'ggheheghbfbmbmfwe', '2015-09-25 14:15:49'),
(211, 'admin', 'this is a sample post', '2015-09-25 14:22:22'),
(212, 'admin', 'this is a sample post', '2015-09-25 14:22:22'),
(213, 'geekgirl', 'hello am new here', '2015-09-25 14:24:09'),
(215, 'admin', 'this is a sample posthello there', '2015-09-25 14:37:36'),
(216, 'admin', 'do these assignments', '2015-09-25 14:38:14'),
(218, 'admin', 'hi', '2015-09-25 15:29:09'),
(220, 'geekly', 'am a new member\r\n', '2016-01-26 21:47:54'),
(221, 'someone', 'Helloo...am new here :)', '2016-08-04 12:13:11'),
(223, 'someone', 'This is cool\r\n', '2016-08-04 12:13:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other`
--
ALTER TABLE `other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `other`
--
ALTER TABLE `other`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=224;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
