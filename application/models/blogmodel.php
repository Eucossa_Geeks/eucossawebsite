 <?php

	class Blogmodel extends CI_Model
	{
		

		function get_records()
		{
			$query=$this->db->get('member');
			return $query->result();
		}

		function add_record($member)
		{

			$this->db->insert('member',$member);
		}

		function add_record_posts($posts)
		{
			$query=$this->db->get('posts');

			$this->db->insert('posts',$posts);
		}

		function add_record_other($other)
		{
			$query=$this->db->get('other');

			$this->db->insert('other',$other);
		}


		function check_exists_username($username)
		{
			$query_str="SELECT username from member where username=?";
			$result= $this->db->query($query_str, $username);

			if ($result->num_rows()>0) {
				//username already exists
				return true;
			}
			else
			{
				//username doesnt exist
				return false;
			}
		}

		function check_login($username,$pass_word)  //login validation
		{
			$pass_word=md5($pass_word);
			$username=$this->input->post('username');
			$query_str="SELECT id FROM member WHERE username=? and pass_word=?";
			$result=$this->db->query($query_str, array($username,$pass_word));

			if ($result->num_rows()==1) {
				return $result->row(0);
			}
			else
			{
				return false;
			}

		   
		}

		function update_password($member)
		{
			$query=$this->db->get('member');
			$this->db->where("username",$this->input->post('username'));
			$this->db->update("member",$member);
		}

		function delete_row()
		{
			$username=$this->input->post('username');
			$query=$this->db->get('posts');
			$this->db->where("username",$username);
			$this->db->where("id",$this->uri->segment(3));
			$this->db->delete("posts");
			
		}

		function delete_Qrow()
		{
			$username=$this->input->post('username');
			$query=$this->db->get('other');
			$this->db->where("username",$username);
			$this->db->where("id",$this->uri->segment(3));
			$this->db->delete("other");
		}

		function delete_mem()
		{
			$query=$this->db->get('member');
			$this->db->where("id",$this->uri->segment(3));
			$this->db->delete("member");
		}

		function delete_msg()
		{
			$query=$this->db->get('messages');
			$this->db->where("id",$this->uri->segment(3));
			$this->db->delete("messages");
		}



		function message($message)
		{
			$query=$this->db->get('messages');

			$this->db->insert('messages',$message);
		}


		function check_exists_user($username)
		{
			$query_str="SELECT username from member where username=?";
			$result= $this->db->query($query_str, $username);

			if ($result->num_rows()>0) {
				//username already exists
				return true;
			}
			else
			{
				//username doesnt exist
				return false;
			}
		}

		


	}

?>