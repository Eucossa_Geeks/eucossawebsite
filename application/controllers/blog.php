
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Blog extends CI_Controller 
{

		
		function logout()
		{
			$this->session->sess_destroy();
			redirect('welcome/index');
		}



		 function signup()
		{ 

			 $this->load->library('form_validation');
			 $this->form_validation->set_rules('username','Username','trim|required|min_length[3]|xss_clean|strtolower|callback_username_not_exists');
			 $this->form_validation->set_rules('pass_word','Password','trim|required|xss_clean');
			 $this->form_validation->set_rules('password2','Password Confirmation','trim|required|matches[pass_word]|xss_clean');




			  if ($this->form_validation->run()==FALSE) {
			  	//hasnt been run or there are errors
			  	$this->load->view('index.html');
			  }
			  else
			  {
			  	
			  	$member=array(
				'username'=> $this->input->post('username'), 
				'pass_word'=>md5($this->input->post('pass_word')),
				'password2'=>md5($this->input->post('password2')),
				);
				$this->blogmodel->add_record($member);
				$this->load->view('registered.html');

			  }
			  
			
			
		}



		function username_not_exists($username)
		{
			$this->form_validation->set_message('username_not_exists','The %s you picked exists :(  Please choose another username.');

			if ($this->blogmodel->check_exists_username($username)) {
				return false;
			}
			else
			{
				return true;
			}
		}

		function login()
		{
			$this->load->model('blogmodel');
			$username=$this->input->post('username');
			$pass_word=$this->input->post('pass_word');
			$id=$this->blogmodel->check_login($username,$pass_word);
			
			

			if (!$id) {
				//logged in failed
				$this->session->set_flashdata('login_error', TRUE);
				redirect('welcome/index');
					}
			else
		{
			

			if ($username=='admin') 
				{
					$this->session->set_userdata(array(
					'logged_in'=>TRUE,
					'id'=>$id->id
					
					));
					$this->load->view('admin.html');
				}

				else
				{
				$this->session->set_userdata(array(
				'logged_in'=>TRUE,
				'id'=>$id->id
				));
				$this->load->view('member.html');}	

			}
		}

	



		function posts()
		{
		

			$posts=array(
				'username'=>$this->input->post('username'),
				'content'=>$this->input->post('content'),
				'eventdate'=>$this->input->post('eventdate'),

				);
					
				$this->blogmodel->add_record_posts($posts);
				
				$username=$this->input->post('username');


				$this->add();	


		}



		function add()
		{
			$username=$this->input->post('username');


				if ($username=='admin') 
				{
					$this->session->set_userdata(array(
					'logged_in'=>TRUE
					
					));
					$this->load->view('admin.html');
				}

				else
				{
				$this->session->set_userdata(array(
				'logged_in'=>TRUE
				));
				$this->load->view('member.html');}	

		}


		function other()
		{
			$other=array(
				'username'=> $this->input->post('username'), 
				'content'=>$this->input->post('content'),
				'eventdate'=>$this->input->post('eventdate')
				);	
				$this->blogmodel->add_record_other($other);
				
				$this->add();	



		}

		function update_password()
		{
			 $this->load->library('form_validation');

			 $this->form_validation->set_rules('pass_word','Password','trim|required|xss_clean');
			 $this->form_validation->set_rules('password2','Password Confirmation','trim|required|matches[pass_word]|xss_clean');




			  if ($this->form_validation->run()==true) {

					$member=array(
						'pass_word'=>md5($this->input->post('pass_word')),
						'password2'=>md5($this->input->post('password2')),
						);

					$this->blogmodel->update_password($member);
					

				$this->load->view('admin.html');


		}

		else
		{
					$this->load->view('admin.html');
		}


		}  

		function delete()
		{
			$username=$this->input->post('username');
			$this->blogmodel->delete_row();
			$this->load->view('delpost.html');

		}

		function deleteQ()
		{
			$this->blogmodel->delete_Qrow();
			$this->load->view('delpost.html');
		}

		function delete_mem()
		{
			$this->blogmodel->delete_mem();
			$this->load->view('delmem.html');
		}

		function delete_msg()
		{
			$this->blogmodel->delete_msg();
			$this->load->view('delmsg.html');
		}



		function user_not_exists($username)
		{
			$this->form_validation->set_message('user_not_exists','There is no user by that username!. Please confirm that name and try again ');

			if ($this->blogmodel->check_exists_user($username)) {
				return true;
			}
			else
			{
				return false;
			}
		}



		function send_message()
		{
			 $this->load->library('form_validation');
			 $this->form_validation->set_rules('username','Username','trim|required|min_length[3]|xss_clean|strtolower|callback_user_not_exists');
			
			  if ($this->form_validation->run()==TRUE) {

			$message=array(
				'username'=> $this->input->post('username'), 
				'message'=>$this->input->post('message'),
				'eventdate'=>$this->input->post('eventdate'),
				'from'=>$this->input->post('from')
				);	
			$this->blogmodel->message($message);
			$this->load->view('msgsent.html');
				
				}
				else
				{
					$this->load->view('msgfail.html');
				}
		}




		



	}



?>